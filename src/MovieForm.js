import React, { Component } from 'react';
import MovieItems from './MovieItems';

class MovieForm extends Component {

	constructor(props) {
		super(props);

		this.state = {
			items: []
		};

		this.addItem = this.addItem.bind(this);
		this.deleteItem = this.deleteItem.bind(this);
	}

	addItem(e) {
		if (this._inputElement.value !== "") {

			let newItem = {
				titleText: 	this._inputElement.value,
				genreText: 	this._selectElement.value,
				key: 		Date.now()
			};

			this.setState((prevState) => {
				return { 
					items: prevState.items.concat(newItem) 
				};
			});

			this._inputElement.value = "";
			this._selectElement.value = "";
		}

		console.log(this.state.items);
		
		e.preventDefault();
	}
	
	deleteItem(key) {
		let filteredItems = this.state.items.filter(function (item) {
			return (item.key !== key);
		});

		this.setState({
			items: filteredItems
		});
	}

	render() {
		return(

			<div className="movie-form">

				<form onSubmit={this.addItem}>
					<div className="form-item">
						<span>I love the movie</span>
						<input type="text" ref={(a) => this._inputElement = a} required />,<br/>
						<span>which is a/an</span>
						<div className="form-select">
							<select ref={(a) => this._selectElement = a} required>
								<option></option>
								<option>action</option>
								<option>animation</option>
								<option>comedy</option>
								<option>drama</option>
								<option>horror</option>
								<option>foreign</option>
								<option>tragedy</option>
								<option>western</option>
							</select>
						</div>
						<span>film.</span>
						<button type="submit">SAVE IT!</button>
					</div>
				</form>

				<MovieItems entries={this.state.items} delete={this.deleteItem} />

			</div>

		)
	}
}

export default MovieForm;