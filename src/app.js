import React, { Component } from 'react';

import './app.css';
import MovieForm from './MovieForm';

class App extends Component {
	render(){
		return (
			<MovieForm />
		)
	}
}

export default App;