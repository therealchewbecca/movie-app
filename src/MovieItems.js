import React, { Component } from 'react';
import StarRatings from './StarRatings';
 
class MovieItems extends Component {

	constructor(props) {
		super(props);

		this.createItems = this.createItems.bind(this);
	}

	delete(key) {
		this.props.delete(key);
	}

	createItems(item) {
		return (
			<li key={item.key}>
				<span className="title">{item.titleText}</span>
				<span className="genre">{item.genreText}</span>
				<StarRatings />
				<span className="delete" onClick={() => this.delete(item.key)} key={item.key}>x</span>
			</li>
		);
	}

	render() {
		let movieEntries = this.props.entries;
		let listItems = movieEntries.map(this.createItems);

		return (
			<ul className="movie-list">
				{listItems}
			</ul>
		);
	}

};
 
export default MovieItems;