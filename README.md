This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), running Node v10.13.0. It also utilizes the [React Star Rating Component](https://www.npmjs.com/package/react-star-rating-component).

## View the project

[http://old-grape.surge.sh/](http://old-grape.surge.sh/)

## Run the project locally

In the project directory, you can run:

### `npm start`

Runs the app in the development mode. 
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

